Notify
======

Provides asynchronous notifications via TYPO3 flash message technology for website users. Making the notifications
asynchronous helps to establish fully cached pages.

## Usage

Include the public script in your page templates:

```html
<f:if condition="{pagedata.enable_notification}">
    <v:asset movable="false" standalone="true" name="vitdNotify">
        <script
            src="{f:uri.resource(extensionName: 'notify', path: 'notify.js')}"
            async="true"
            data-endpoint="{h:viewHelpers.uri.ajaxAction(extensionName: 'notify', pluginName: 'List', controller: 'Message', action: 'list', absolute: 1)}"
        ></script>
    </v:asset>
</f:if>
```

and register your own listeners:

```javascript
window.VITD = window.VITD || {};
window.VITD.notify = window.VITD.notify || {};
window.VITD.notify.listeners = window.VITD.notify.listeners || {};
/**
 * Notify website users
 *
 * @var {String} message Notification message
 * @var {String} title Notification title (optional, might be null or undefined)
 * @var {string} severity Notification severity. One of 'notice', 'info', 'success', 'warning' or 'danger'
 */
window.VITD.notify.listeners.myListener = function(message, title, severity){
    // display / handle the notification
    // (you can choose to handle notifications based on severity)
};
```

If you do not attach any listeners a default one will be installed, logging to the browser console.
