<?php
defined('TYPO3_MODE') or die();

(function (string $table) {

    // ------------------------- additional columns -------------------------
    $GLOBALS['TCA'][$table]['columns']['enable_notification'] = [
        'exclude' => true,
        'label' => 'Auf dieser Seite Benachrichtigungen abfragen und anzeigen',
        'config' => [
            'type' => 'check',
            'default' => 1,
        ],
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility
        ::addToAllTCAtypes($table,'enable_notification', '1', 'after:php_tree_stop');
})('pages');
