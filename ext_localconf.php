<?php
defined('TYPO3_MODE') or die();

(function ($packageKey) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'VITD.' . $packageKey,
        'List',
        ['Message' => 'list'],
        ['Message' => 'list']
    );
})('notify');
