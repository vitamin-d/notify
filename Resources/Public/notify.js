'use strict';
(function(window, document, endpoint) {
	(function(cb) {
		if (document.readyState === 'interactive' || document.readyState === 'complete') {
			cb.call();
		} else {
			document.addEventListener('readystatechange', function() {
				if (document.readyState === 'interactive') {
					cb.call();
				}
			});
		}
	})(function() {
		window.VITD = window.VITD || {};
		window.VITD.notify = window.VITD.notify || {};
		if (typeof window.VITD.notify.listeners === 'undefined') {
			window.VITD.notify.listeners = {
				'console': function(message, severity, title) {

					switch(severity) {
						case 'info':
						case 'success':
							console.info('%c' + title, 'font-weight: bold;', message);
							break;
						case 'warning':
							console.warn('%c' + title, 'font-weight: bold;', message);
							break;
						case 'danger':
							console.error('%c' + title, 'font-weight: bold;', message);
							break;
						// case 'notice':
						default:
							console.log('%c' + title, 'font-weight: bold;', message);
							break;
					}
				}
			};
		}

		var xhr = new XMLHttpRequest();
		xhr.addEventListener('load', function() {
			var messages = JSON.parse(this.responseText);
			for (var i = 0; i < messages.length; i++) {
				var severity = (typeof messages[i].severity === 'undefined') ? 'success' : messages[i].severity;
				for (var listener in window.VITD.notify.listeners) {
					if (window.VITD.notify.listeners.hasOwnProperty(listener)) {
						window.VITD.notify.listeners[listener].call(this, messages[i].message, severity, messages[i].title);
					}
				}
			}
		});
		xhr.open('GET', endpoint);
		xhr.setRequestHeader('Accept', 'application/json');
		xhr.send();
	});
})(window, document, document.currentScript.dataset.endpoint);
