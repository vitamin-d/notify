TODO
====

## Query identifier handling

Since TYPO3 8 the global notification queue is gone. What's more, it is quite hard to even
_willfully_ get all pending notifications (you'd need to know all _notification namespaces_ for that).
The core idea of this extension would be to display all notifications. Ouch!

## Client side events

The current javascript file is not well written. The notifications should be dispatched as events instead.
