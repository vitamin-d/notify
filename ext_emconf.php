<?php
$EM_CONF['notify'] = [
    'title' => 'Asynchronous notifications',
    'description' => 'Provides asynchronous notifications via TYPO3 flash message technology for website users. '
        . 'Helps to establish fully cached pages.',
    'category' => 'misc',
    'author' => 'Ludwig Rafelsberger',
    'author_email' => 'ludwig.rafelsberger@vitd.at',
    'author_company' => 'VITAMIN D GmbH',
    'state' => 'stable',
    'version' => '87.0.0',
    'constraints' => [
        'depends' => [
            'php' => '7.0.0-7.99.99',
            'typo3' => '8.7.0-8.7.99',
            'fluid_styled_content' => '8.7.0-8.7.99',
            'typoscript_rendering' => '2.0.0-2.0.99',
        ],
    ],
];
