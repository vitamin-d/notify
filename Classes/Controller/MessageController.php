<?php
namespace VITD\Notify\Controller;

use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Web;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Controller for Messages
 */
class MessageController extends ActionController
{
    /**
     * List messages
     *
     * @param bool $flush Whether to flush (true) the messages after listing or not (false)
     *
     * @return string JSON-encoded list of messages
     */
    public function listAction(bool $flush = true): string
    {
        $messages = [];
        // TODO: implement message queue specification and wildcard
        $messageQueue = GeneralUtility::makeInstance(FlashMessageService::class)->getMessageQueueByIdentifier();
        $flashMessages = $flush
            ? $messageQueue->getAllMessagesAndFlush()
            : $messageQueue->getAllMessages();
        foreach($flashMessages as $message) {
            $t = [
                'message' => $message->getMessage(),
                'severity' => [-2 => 'notice', -1 => 'info', 0 => 'success', 1 => 'warning', 2 => 'danger'][$message->getSeverity()],
            ];
            if ($message->getTitle() !== '') {
                $t['title'] = $message->getTitle();
            }
            $messages[] = $t;
        }

        if ($this->response instanceof Web\Response) {
            // @todo Ticket: #63643 This should be solved differently once request/response model is available for TSFE.
            if (!empty($GLOBALS['TSFE']) && $GLOBALS['TSFE'] instanceof TypoScriptFrontendController) {
                $GLOBALS['TSFE']->setContentType('application/json');
            } else {
                $this->response->setHeader('Content-Type', 'application/json');
            }
        }

        return json_encode($messages, JSON_PRETTY_PRINT);
    }
}
